// handler.processRequest(req, res, htmlData);

module.exports = {
    processRequest: function(req, res, query, htmlData) {
        if (!query.hasOwnProperty('username') || !query.hasOwnProperty('password')) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(htmlData);
            res.end();
        } else {
            legal_db.query('SELECT U_ID, USER_TYPE FROM Login_Information WHERE USERNAME = ? AND PASSWORD = ?', [
                query.username,
                query.password
            ], (err, rows) => {
                if (err) throw err;
                if (rows.length) {
                    // user is logged in
                    if (rows[0].USER_TYPE === 'admin') {
                        res.writeHead(302, {'Location': '/AdminInterface/mainMenuAdmin.html'});
                        res.end();
                    } else {
                        // assume employee
                        res.writeHead(302, {'Location': '/EmployeeInterface/mainMenuEmployee.html'});
                        res.end();
                    }
                } else {
                    console.log('bad password'); 
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write(htmlData);
                    res.end();
                }
            });
        }
    }
};
