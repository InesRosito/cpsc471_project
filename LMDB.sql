CREATE DATABASE IF NOT EXISTS `LEGAL` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `LEGAL`;

-- ---------------------------- BEGIN EMPLOYEE ---------------------------- --

--
-- Table structure for table `Employee`
--
DROP TABLE IF EXISTS `Employee`;
CREATE TABLE `Employee` (
	`E_ID` INT NOT NULL AUTO_INCREMENT, 			-- Employee ID (Implemented for Security) & increases with each Employee added
    `E_SIN` INT NOT NULL, 									-- Employee's SIN
    `E_POSITION` VARCHAR(50) NOT NULL,			-- Employee's current position in Company
    `NAME` VARCHAR(100) NOT NULL, 					-- Employee's name
    `ACCOUNT_NO` INT DEFAULT NULL, 					-- Account No. refers to what?
	`PAYMENT_METHOD` VARCHAR(15) DEFAULT NULL, 	-- Presume this refers to payment of salary
    `EMAIL` VARCHAR(50) NOT NULL, 					-- Company email for employee
    `DOB` DATE, 														-- Date of Birth of Employee
    `ADDRESS` VARCHAR(100) DEFAULT NULL, 		-- Address of Employee
    `HOURLY_SAL` DOUBLE DEFAULT NULL, 			-- Employee's Hourly Salary
    `YEARLY_SAL` DOUBLE DEFAULT NULL, 			-- Employee's Yearly Salary
	UNIQUE(`E_ID`, `E_SIN`),
    PRIMARY KEY (`E_ID`, `E_SIN`)
) ENGINE=InnoDB;


--
-- Table structure for table `Login_Information`
--
DROP TABLE IF EXISTS `Login_Information`;
CREATE TABLE `Login_Information` (
	`USERNAME` VARCHAR(30) NOT NULL,	-- Username for Login
    `PASSWORD` VARCHAR(30) NOT NULL,	-- Password for Login
    `U_ID` INT NOT NULL,				-- UID, users ID in Employee/Client table
    `USER_TYPE` VARCHAR(50) DEFAULT NULL,		-- Employee's current position in Company
    CONSTRAINT `U_ID` FOREIGN KEY (`U_ID`) REFERENCES `Employee` (`E_ID`)
) ENGINE=InnoDB;


--
-- Table structure for table `Employee_Phone`
--
DROP TABLE IF EXISTS `Employee_Phone`;
CREATE TABLE `Employee_Phone` (
	`E_ID` INT NOT NULL,						-- Employee ID
	`E_NUMBER` INT NOT NULL,					-- One of Employee's Phone/Fax Number
    `E_NUMBER_TYPE` VARCHAR(5) NOT NULL,		-- Identifying if `E_NUMBER` is a phone or a fax number
    PRIMARY KEY (`E_NUMBER`),
	CONSTRAINT `Phone_EID` FOREIGN KEY (`E_ID`) REFERENCES `Employee` (`E_ID`)
) ENGINE=InnoDB;


-- ---------------------------- END EMPLOYEE ---------------------------- --


-- ---------------------------- BEGIN CLIENT ---------------------------- --
--
-- Table structure for table `Client`
--
DROP TABLE IF EXISTS `Client`;
CREATE TABLE `Client` (
	`C_ID` INT NOT NULL AUTO_INCREMENT, 	-- Client ID (Implemented for Security) & increases with each Client added
	`C_SIN` INT NOT NULL, 					-- Client SIN
    `Name` VARCHAR(100) NOT NULL, 			-- Client Name
    `EMAIL` VARCHAR(50) DEFAULT NULL,		-- Client Email
	`NOTES` MEDIUMTEXT DEFAULT NULL, 		-- Notes provided by Client
    `DOB` DATE NOT NULL,					-- Date of Birth of Client
    `ADDRESS` VARCHAR(100),					-- Address of Client
    UNIQUE(`C_ID`, `C_SIN`),
	PRIMARY KEY (`C_ID`, `C_SIN`)
) ENGINE=InnoDB;


--
-- Table structure for table `Client_Phone`
--
DROP TABLE IF EXISTS `Client_Phone`;
CREATE TABLE `Client_Phone` (
	`C_ID` INT NOT NULL,						-- Client ID
	`C_PHONE` INT NOT NULL,						-- One of Client's Phone/Fax Number
    `C_NUMBER_TYPE` VARCHAR(5) NOT NULL,		-- Identifying if `C_NUMBER` is a phone or a fax number
    PRIMARY KEY (`C_PHONE`),
	CONSTRAINT `Phone_CID` FOREIGN KEY (`C_ID`) REFERENCES `Client` (`C_ID`)
) ENGINE=InnoDB;


-- Table structure for table `Employee_Works_With_Client`
--
DROP TABLE IF EXISTS `Employee_Works_With_Client`;
CREATE TABLE `Employee_Works_With_Client` (
	`E_ID` INT NOT NULL, 			-- Employee ID associated with a Client
	`C_ID` INT NOT NULL,			-- Client ID
	PRIMARY KEY (`C_ID`),
   	CONSTRAINT `EClient_EID` FOREIGN KEY (`E_ID`) REFERENCES `Employee`(`E_ID`),
   	CONSTRAINT `EClient_CID` FOREIGN KEY (`C_ID`) REFERENCES `Client`(`C_ID`)
) ENGINE=InnoDB;
-- ---------------------------- END CLIENT ---------------------------- --



-- ---------------------------- BEGIN REFERENCE MATERIAL ---------------------------- --
--
-- Table structure for table `Reference Material`
--
DROP TABLE IF EXISTS `Reference_Material`;
CREATE TABLE `Reference_Material` (
	`R_ID` INT NOT NULL AUTO_INCREMENT, 	-- Reference ID
    `TITLE` VARCHAR(50) NOT NULL,			-- Reference Title
    `PATH` VARCHAR(100) NOT NULL, 			-- Reference Path location
    `DOC_TYPE` VARCHAR(5) NOT NULL,			-- Reference document type
    UNIQUE(`R_ID`),
    PRIMARY KEY(`R_ID`, `TITLE`)
) ENGINE=InnoDB;


--
-- Table structure for table `Reference_Material_Regards_Client`
--
DROP TABLE IF EXISTS `Reference_Material_Regards_Client`;
CREATE TABLE `Reference_Material_Regards_Client` (
	`C_ID` INT NOT NULL, 				-- Client ID
	`R_ID` INT NOT NULL,		-- Reference Material associated with a Client
	PRIMARY KEY (`R_ID`),
	CONSTRAINT `CRef_CID` FOREIGN KEY (`C_ID`) REFERENCES `Client`(`C_ID`),
   	CONSTRAINT `CRef_RID` FOREIGN KEY (`R_ID`) REFERENCES `Reference_Material`(`R_ID`)
) ENGINE=InnoDB;


--
-- Table structure for table `Employee_Has_Reference_Material`
--
DROP TABLE IF EXISTS `Employee_Has_Reference_Material`;
CREATE TABLE `Employee_Has_Reference_Material` (
	`E_ID` INT NOT NULL, 				-- Employee ID
	`R_ID` INT NOT NULL,		-- Reference Material available to Employee
	PRIMARY KEY (`R_ID`),
	CONSTRAINT `ERef_EID` FOREIGN KEY (`E_ID`) REFERENCES `Employee`(`E_ID`),
   	CONSTRAINT `ERef_RID` FOREIGN KEY (`R_ID`) REFERENCES `Reference_Material`(`R_ID`)
) ENGINE=InnoDB;


--
-- Table structure for table `Reference_Material_Subject`
--
DROP TABLE IF EXISTS `Reference_Material_Subject`;
CREATE TABLE `Reference_Material_Subject` (
	`R_ID` INT NOT NULL, 		-- Reference ID
	`Subject` VARCHAR(50) NOT NULL,		-- Subject tag associated with Reference Material
    PRIMARY KEY (`R_ID`, `Subject`),
   	CONSTRAINT `SRef_RID` FOREIGN KEY (`R_ID`) REFERENCES `Reference_Material`(`R_ID`)
) ENGINE=InnoDB;
-- ---------------------------- END REFERENCE MATERIAL ---------------------------- --



-- ---------------------------- BEGIN APPOINTMENTS ---------------------------- --
--
-- Table structure for table `Appointments`
--
DROP TABLE IF EXISTS `Appointments`;
CREATE TABLE `Appointments` (
	`APP_ID` INT NOT NULL AUTO_INCREMENT,	-- Appointment ID number
    `DATE` DATE NOT NULL, 					-- Date of Appointment
    `TITLE` VARCHAR(50) NOT NULL, 			-- Appointment Name
    `NOTES` MEDIUMTEXT DEFAULT NULL, 		-- Appointment Notes
	`LOCATION` VARCHAR(100) DEFAULT NULL, 	-- Location of Appointment
    PRIMARY KEY (`APP_ID`, `DATE`)
) ENGINE=InnoDB;

--
-- Table structure for table `Employee_Has_Appointment`
--
DROP TABLE IF EXISTS `Employee_Has_Appointment`;
CREATE TABLE `Employee_Has_Appointment` (
	`APP_ID` INT NOT NULL, 				-- Appointment ID number
    `E_ID` INT NOT NULL,				-- ID of Employee attending appointment
    PRIMARY KEY (`APP_ID`, `E_ID`),
	CONSTRAINT `A_AID` FOREIGN KEY (`APP_ID`) REFERENCES `Appointments`(`APP_ID`),
	CONSTRAINT `A_EID` FOREIGN KEY (`E_ID`) REFERENCES `Employee`(`E_ID`)
)ENGINE=InnoDB;


--
-- Table structure for table `Client_Has_Appointment`
--
DROP TABLE IF EXISTS `Client_Has_Appointment`;
CREATE TABLE `Client_Has_Appointment` (
	`APP_ID` INT NOT NULL, 				-- Appointment ID number
    `C_ID` INT NOT NULL,				-- ID of Client attending appointment
    PRIMARY KEY (`APP_ID`, `C_ID`),
	CONSTRAINT `APP_AID` FOREIGN KEY (`APP_ID`) REFERENCES `Appointments`(`APP_ID`),
	CONSTRAINT `A_CID` FOREIGN KEY (`C_ID`) REFERENCES `Client`(`C_ID`)
)ENGINE=InnoDB;




-- ---------------------------- END APPOINTMENTS ---------------------------- --


-- ---------------------------- BEGIN CONTACT ---------------------------- --
--
-- Table structure for table `Contact`
--
DROP TABLE IF EXISTS `Contact`;
CREATE TABLE `Contact` (
	`CONTACT_ID` INT NOT NULL AUTO_INCREMENT, 	-- Contact ID
    `NAME` VARCHAR(100) NOT NULL, 				-- Contact Name
	`NOTES` MEDIUMTEXT DEFAULT NULL, 			-- (??)
	`TYPE` VARCHAR(20) NOT NULL, 				-- (??)
    `COMPANY` VARCHAR(50) DEFAULT NULL,  		-- Company associated with Contact
    `EMAIL` VARCHAR(50) DEFAULT NULL,  			-- Email of Contact
    `ADDRESS` VARCHAR(100) DEFAULT NULL, 		-- Address of Contact
    UNIQUE(`CONTACT_ID`),
    PRIMARY KEY(`CONTACT_ID`)
) ENGINE=InnoDB;


--
-- Table structure for table `Contact_Associated_With_Client`
--
DROP TABLE IF EXISTS `Contact_Associated_With_Client`;
CREATE TABLE `Contact_Associated_With_Client` (
	`C_ID` INT NOT NULL, 						-- Client ID
	`CONTACT_ID` INT NOT NULL,					-- Contact ID
    PRIMARY KEY (`C_ID`, `CONTACT_ID`),
	CONSTRAINT `CC_CID` FOREIGN KEY (`C_ID`) REFERENCES `Client`(`C_ID`),
    CONSTRAINT `CC_ContactID` FOREIGN KEY (`CONTACT_ID`) REFERENCES `Contact`(`CONTACT_ID`)
) ENGINE=InnoDB;


--
-- Table structure for table `Contact_Phone`
--
DROP TABLE IF EXISTS `Contact_Phone`;
CREATE TABLE `Contact_Phone` (
	`CONTACT_ID` INT NOT NULL,						-- Contact ID
	`CONTACT_NUMBER` INT NOT NULL,					-- One of Contact's Phone/Fax Number
    `CONTACT_NUMBER_TYPE` VARCHAR(5) NOT NULL,		-- Identifying if `CONTACT_NUMBER` is a phone or a fax number
    PRIMARY KEY (`CONTACT_NUMBER`),
	CONSTRAINT `Phone_ContactID` FOREIGN KEY (`CONTACT_ID`) REFERENCES `Contact` (`CONTACT_ID`)
) ENGINE=InnoDB;



--
-- Table structure for table `Contact_Has_Appointment`
--
DROP TABLE IF EXISTS `Contact_Has_Appointment`;
CREATE TABLE `Contact_Has_Appointment` (
	`APP_ID` INT NOT NULL, 					-- Appointment ID number
    `CONTACT_ID` INT NOT NULL,				-- ID of Contact attending appointment
    PRIMARY KEY (`APP_ID`, `CONTACT_ID`),
	CONSTRAINT `AC_AID` FOREIGN KEY (`APP_ID`) REFERENCES `Appointments`(`APP_ID`),
    CONSTRAINT `A_ContactID` FOREIGN KEY (`CONTACT_ID`) REFERENCES `Contact`(`CONTACT_ID`)
)ENGINE=InnoDB;
-- ---------------------------- END CONTACT ---------------------------- --


-- ---------------------------- BEGIN LEGAL CASE ---------------------------- --
--
-- Table structure for table `Legal_File`
--
DROP TABLE IF EXISTS `Legal_File`;
CREATE TABLE `Legal_File` (
	`FILE_NUMBER` INT NOT NULL AUTO_INCREMENT, 		-- ID for Legal File (Case)
	`FILE_STATUS` VARCHAR(50) NOT NULL, 			-- Legal File status
    `NOTES` MEDIUMTEXT DEFAULT NULL, 				-- Notes on Legal File
	`TYPE_OF_LAW` VARCHAR(20) NOT NULL, 			-- Type of Law associated with Legal File
    `DATE_STARTED` DATE NOT NULL, 					-- Start date of case
    `DATE_ENDED` DATE DEFAULT NULL, 				-- End date of case
    `C_ID` INT NOT NULL,							-- Client ID associated with case
    UNIQUE (`FILE_NUMBER`),
    PRIMARY KEY (`FILE_NUMBER`),
	CONSTRAINT `File_CID` FOREIGN KEY (`C_ID`) REFERENCES `Client` (`C_ID`)
) ENGINE=InnoDB;


--
-- Table structure for table `Employee_Has_Case` (Case = Legal File)
--
DROP TABLE IF EXISTS `Employee_Has_Case`;
CREATE TABLE `Employee_Has_Case` (
	`FILE_NUMBER` INT NOT NULL,  				-- Legal File ID
    `E_ID` INT NOT NULL,					-- ID of Employee working Legal File
    PRIMARY KEY (`FILE_NUMBER`, `E_ID`),
	CONSTRAINT `F_FileNum` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File`(`FILE_NUMBER`),
   	CONSTRAINT `F_EID` FOREIGN KEY (`E_ID`) REFERENCES `Employee`(`E_ID`)
) ENGINE=InnoDB;
-- ---------------------------- END LEGAL CASE ---------------------------- --


-- ---------------------------- BEGIN DOCUMENTS ---------------------------- --
--
-- Table structure for table `Documents`
-- OR DO WE WANT TO SET THIS UP LIKE REFERENCE MATERIAL??
--
DROP TABLE IF EXISTS `Documents`;
CREATE TABLE `Documents` (
	`TITLE` VARCHAR(50) NOT NULL, 	-- Document Title
    `C_ID` INT NOT NULL, 			-- ID of Client associated with Document
	`TYPE` VARCHAR(20) NOT NULL, 	-- Document type
    `CONTENT` TEXT NOT NULL,		-- Document Content
    PRIMARY KEY(`TITLE`),
	CONSTRAINT `File_and_CID` FOREIGN KEY (`C_ID`) REFERENCES `Client` (`C_ID`)
) ENGINE=InnoDB;
-- ---------------------------- END DOCUMENTS ---------------------------- --


-- ---------------------------- BEGIN COMMUNICATION RECORDS ---------------------------- --
--
-- Table structure for table `Communication_Records`
--
DROP TABLE IF EXISTS `Communication_Records`;
CREATE TABLE `Communication_Records` (
	`COMM_ID` INT NOT NULL AUTO_INCREMENT,	-- Communication record ID
    `FILE_NUMBER` INT NOT NULL,				-- Legal Case File number
	`TITLE` VARCHAR(20) NOT NULL,			-- Title of Communication Record
    `CONTENT` TEXT DEFAULT NULL,			-- Content of the Communication Record
    `METHOD` VARCHAR(100) NOT NULL,			-- Method in which Communication occured
    `TIME` TIMESTAMP NOT NULL, 				-- Timestamp of Communication Record
    `DATE` DATE NOT NULL, 					-- Date of Communication Record
    UNIQUE(`COMM_ID`),
    PRIMARY KEY (`COMM_ID`),
   	CONSTRAINT `CR_FileNum` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File` (`FILE_NUMBER`)
) ENGINE=InnoDB;


--
-- Table structure for table `Client_Has_Comm_Records`
--
DROP TABLE IF EXISTS `Client_Has_Comm_Records`;
CREATE TABLE `Client_Has_Comm_Records` (
	`COMM_ID` INT NOT NULL, 				-- Communication record ID
    `C_ID` INT DEFAULT NULL,				-- ID of Client associated with Communication Record
    `FILE_NUMBER` INT NOT NULL,
    PRIMARY KEY (`COMM_ID`),
   	CONSTRAINT `CR_cli_FileNum` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File` (`FILE_NUMBER`),
   	CONSTRAINT `CR_CID` FOREIGN KEY (`C_ID`) REFERENCES `Client` (`C_ID`)
) ENGINE=InnoDB;


--
-- Table structure for table `Contact_Has_Comm_Records`
--
DROP TABLE IF EXISTS `Contact_Has_Comm_Records`;
CREATE TABLE `Contact_Has_Comm_Records` (
	`COMM_ID` INT NOT NULL AUTO_INCREMENT,	-- Communication record ID
    `CONTACT_ID` INT DEFAULT NULL,			-- ID of Contact associated with Communication Record
    `FILE_NUMBER` INT NOT NULL,
    PRIMARY KEY (`COMM_ID`),
   	CONSTRAINT `CR_cont_FileNum` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File` (`FILE_NUMBER`),
   	CONSTRAINT `CR_ContactID` FOREIGN KEY (`Contact_ID`) REFERENCES `Contact` (`Contact_ID`)
) ENGINE=InnoDB;
-- ---------------------------- END COMMUNICATION RECORDS ---------------------------- --


-- ---------------------------- BEGIN ACCCOUNTING ---------------------------- --
--
-- Table structure for table `Trust_Fund`
--
DROP TABLE IF EXISTS `Trust_Fund`;
CREATE TABLE `Trust_Fund` (
	`FILE_NUMBER` INT NOT NULL, 		-- Number of Case File
	`ACCOUNT_NUMBER` INT NOT NULL, 		-- Account Number
    `FEE_PERCENTAGE` DOUBLE NOT NULL, 	-- (??)
    `TOTAL_AMOUNT` DOUBLE NOT NULL,		-- (??)
    PRIMARY KEY(`FILE_NUMBER`),
	CONSTRAINT `TF_FileNum` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File` (`FILE_NUMBER`)
) ENGINE=InnoDB;


--
-- Table structure for table `Fees`
--
DROP TABLE IF EXISTS `Fees`;
CREATE TABLE `Fees` (
	`FILE_NUMBER` INT NOT NULL, 	-- Number of Case File
    `TOTAL` DOUBLE NOT NULL,		-- (??)
    CONSTRAINT `Fee_FileNum` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File` (`FILE_NUMBER`)
) ENGINE=InnoDB;


--
-- Table structure for table `Billable_Hours`
--
DROP TABLE IF EXISTS `Billable_Hours`;
CREATE TABLE `Billable_Hours`(
	`BILL_ID` INT NOT NULL AUTO_INCREMENT,	-- ID for Billable Hours
    `FILE_NUMBER` INT NOT NULL, 			-- ID of Legal Case File
	`NUM_OF_HOURS` INT NOT NULL, 			-- Number of hour having worked on File
    `PAYEE` VARCHAR(100) NOT NULL, 			-- Name of individual paying -------------------- OR SHOULD IT BE AN ID (??)
    `ACTIVITY` VARCHAR(100) DEFAULT NULL, 	-- Brief description of what file activity entailed
    `DATE` DATE NOT NULL, 					-- Date of File closed (??)
    `RATE` DOUBLE NOT NULL,					-- Rate of charge
    PRIMARY KEY (`BILL_ID`),
	CONSTRAINT `BH_FileNum` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File` (`FILE_NUMBER`)
) ENGINE=InnoDB;


--
-- Table structure for table `Disbursements`
--
DROP TABLE IF EXISTS `Disbursements`;
CREATE TABLE `Disbursements` (
	`PAYMENT_NUMBER` INT NOT NULL AUTO_INCREMENT, 	-- (??)
    `FILE_NUMBER` INT NOT NULL, 					-- File (Case) Number associated with Disbursement
	`PAYEE` VARCHAR(100) DEFAULT NULL, 				-- Payee associated with Disbursement
    `BASE_COST` DOUBLE NOT NULL, 					-- Base Cost of Disbursement
    `GST` DOUBLE NOT NULL, 							-- GST added to Disbursement
    `TOTAL` DOUBLE NOT NULL, 						-- Total value of transaction
    `DATE` DATE NOT NULL,							-- Date which Disbursement was carried out
    UNIQUE (`PAYMENT_NUMBER`, `FILE_NUMBER`),
    PRIMARY KEY (`PAYMENT_NUMBER`),
	CONSTRAINT `aFile_Number` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File`(`FILE_NUMBER`)
) ENGINE=InnoDB;


--
-- Table structure for table `Paid`
--
DROP TABLE IF EXISTS `Paid`;
CREATE TABLE `Paid` (
	`PAID_ID` INT NOT NULL AUTO_INCREMENT,	-- ID for Payment
    `FILE_NUMBER` INT NOT NULL,				-- ID of Legal Case File
    `AMOUNT` INT NOT NULL, 					-- Amount being paid
    `ACTIVITY` VARCHAR(100) DEFAULT NULL, 	-- Brief description of what file activity entailed
    `DATE` DATE NOT NULL,					-- Date of payment
    PRIMARY KEY (`PAID_ID`, `FILE_NUMBER`),
   	CONSTRAINT `Pay_File_Number` FOREIGN KEY (`FILE_NUMBER`) REFERENCES `Legal_File`(`FILE_NUMBER`)
) ENGINE=InnoDB;
-- ---------------------------- END ACCOUNTING ---------------------------- --
