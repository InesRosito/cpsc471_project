/*
This script is intended to establish the connection to the html pages, as well as
connect to the mysql database.

need to add in references to database to establish password connections, as well
as fix links between the html pages.

database connects to local host port, webserver set up on port 8080
*/


var http = require('http');
var fs = require('fs');
var mysql = require('mysql');
const mysqlUtilities = require('mysql-utilities');
var express = require('express');
var app = express();

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'CATMAN',
  database: 'CPSC471'
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});
app.listen(3307);

http.createServer(function (req, res) {
 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/login.html', function(err, data) {
	//relative path
  fs.readFile('HTML_Files/login.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/mainMenuEmployee.html', function(err, data) {
	// relative path
  fs.readFile('HTML_Files/EmployeeInterface/mainMenuEmployee.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/reference/reference.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/reference/reference.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/calender/calender.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/calender/calender.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/contacts/contacts.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/contacts/contacts.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/contacts/contactInformation.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/contacts/contactInformation.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/contacts/contactNotes.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/contacts/contactNotes.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/contacts/relatedFiles.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/contacts/relatedFiles.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

//  fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/clientInformation.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/files/clientInformation.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/communication.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/files/communication.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/contacts.html', function(err, data) {
 fs.readFile('HTML_Files/EmployeeInterface/files/contacts.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/documents.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/files/documents.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/fileInformation.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/files/fileInformation.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/files.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/files/files.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/notes.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/files/notes.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/reference.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/files/reference.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/EmployeeInterface/files/reference.html', function(err, data) {
  fs.readFile('HTML_Files/EmployeeInterface/files/reference.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });
//AdminInterface---------------------------------------------------------------------------------------------------------------------

 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/mainMenuAdmin.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/mainMenuAdmin.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/reference/reference.html', function(err, data) {
 fs.readFile('HTML_Files/AdminInterface/reference/reference.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/calender/calender.html', function(err, data) {
	 fs.readFile('HTML_Files/AdminInterface/calender/calender.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/contacts/contacts.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/contacts/contacts.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

 // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/contacts/contactInformation.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/contacts/contactInformation.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/contacts/contactNotes.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/contacts/contactNotes.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

//  fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/contacts/relatedFiles.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/contacts/relatedFiles.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

//  fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/clientInformation.html', function(err, data) {
   fs.readFile('HTML_Files/AdminInterface/files/clientInformation.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  // fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/communication.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/files/communication.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/contacts.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/files/contacts.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

//    fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/documents.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/files/documents.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/fileInformation.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/files/fileInformation.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //  fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/files.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/files/files.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //  fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/notes.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/files/notes.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

//  fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/reference.html', function(err, data) { 
 fs.readFile('HTML_Files/AdminInterface/files/reference.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

//  fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/files/reference.html', function(err, data) { 
 fs.readFile('HTML_Files/AdminInterface/files/reference.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

//    fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/employees/employees.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/employees/employees.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });

  //  fs.readFile('/Users/Neera/Documents/CPSC_471_proj/HTML_Files/AdminInterface/employees/employeeInformation.html', function(err, data) {
  fs.readFile('HTML_Files/AdminInterface/employees/employeeInformation.html', function(err, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    res.end();
  });


}).listen(8080);
