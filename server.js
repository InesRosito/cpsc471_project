/*******************************************************************************
SERVER.JS

Purpose:
Takes care of connecting to the legal database, and sets up the appropriate
webserver.

*******************************************************************************/

const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const path = require("path");
const url = require("url");
const http = require('http');
const app = express();
const port = 3308;
const fs = require('fs');

global.__root = __dirname;

/*------------------------------------------------------------------------------
This section takes care of connecting to the legal database
------------------------------------------------------------------------------*/
const legal_db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "luchia18",
  database: "legal"
  });

legal_db.connect((err) => {
  if (err) {
    throw err;
    console.log('An error occured when attempting to connect to the database. Please contact the admin');
  }
  console.log('Successful connection to legal');
});
global.legal_db = legal_db;

function getMime(pathname) {
    let extname = path.extname(pathname).substring(1);
    switch (extname) {
        case 'png':
            return 'image/png';
        case 'jpg':
            return 'image/jpeg';
        case 'html': 
            return 'text/html';
        case 'css':
            return 'text/css';
        case 'js':
            return 'application/javascript';
        default:
            console.log(`ERROR: unknown mime for ${pathname}`);
            return 'text/html';
    }
}
    
http.createServer(function (req, res) {
      let reqUrl = url.parse(req.url);
      let filePath = path.resolve(reqUrl.pathname);
      console.log(`[${req.url}] New request: ${req.url}`);
      fs.readFile(path.resolve(`${__root}/HTML_Files/${filePath}`), (err, htmlData) => {
          if (err) {
              // file not found
              console.log(`[${req.url}] Not found: ${filePath}`);
              res.writeHead(404);
              res.end();
          } else {
              console.log(`[${req.url}] Found`);
              let query = {};
              if (reqUrl.query) {
                  let keyValues = reqUrl.query.split('&');
                  for (let keyvalue of keyValues) {
                     let split = keyvalue.split('=');
                     let key = split.splice(0, 1);
                     let value = split.join('=');
                     query[decodeURIComponent(key)] = decodeURIComponent(value);
                  }
              }
              // request to /a.html?foo=bar
              // query = {
              //     foo: bar
              // }

              // if there's also a file called the same thing, but .handler.js after, then load it and call it
              fs.stat(path.resolve(`${__root}/HTML_Files${filePath}.handler.js`), (err, stat) => {
                  if (err) {
                      console.log(`[${req.url}] no handler`);
                      res.writeHead(200, {'Content-Type': getMime(filePath)});
                      res.write(htmlData);
                      res.end();
                  } else {
		      console.log(`[${req.url}] request has a handler`);
		      let handler = require(path.resolve(`${__root}/HTML_Files${filePath}.handler.js`));
		      handler.processRequest(req, res, query, htmlData);
                  }
              });
          }
      });
}).listen(8080);
/*------------------------------------------------------------------------------
app listens to the provided port
------------------------------------------------------------------------------*/
//app.listen(port, () => {
    //console.log(`Server running on port: ${port}`);
//});
