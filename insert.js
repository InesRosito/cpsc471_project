/*
This file allows for a connection to be made to the mysql CPSC471 database
and allows for objects to be inserted into the various tables.

ID values are set to auto increment, so their colums are not specified in the insert
sections, as they will be added automatically by the DBMS.

The connection to the mysql database is made through local host port 3307. 

Need to modify connection to the database, so it is based on the user that enters a given password.
*/


var mysql = require('mysql');
const mysqlUtilities = require('mysql-utilities');
var express = require('express');
var fs = require('fs');
var app = express();

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'CATMAN',
  database: 'CPSC471'
});


connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");


  //ADD EMPLOYEES
  var sql1 = "INSERT INTO Employee (E_SIN, E_POSITION, NAME, ACCOUNT_NO, PAYMENT_METHOD, EMAIL, DOB, ADDRESS, HOURLY_SAL, YEARLY_SAL) VALUES ?";
  var emps = [[]];
  connection.query(sql1, [emps], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD CLIENTS
  var sql2 = "INSERT INTO Client (C_SIN, Name, EMAIL, NOTES, DOB, ADDRESS) VALUES ?";
  var cli = [[]];
  connection.query(sql2, [cli], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD EMPLOYEE PHONE
  var sql3 = "INSERT INTO EMPLOYEE_PHONE (E_ID, E_NUMBER, E_NUMBER_TYPE) VALUES ?";
  var emp_p = [[]];
  connection.query(sql3, [emp_p], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD EMPLOYEE_WORKS_WITH_CLIENT
  var sql4 = "INSERT INTO EMPLOYEE_WORKS_WITH_CLIENT (E_ID, C_ID) VALUES ?";
  var emp_cli = [[]];
  connection.query(sql4, [emp_cli], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD CLIENT_PHONE
  var sql5 = "INSERT INTO CLIENT_PHONE (C_ID, C_PHONE, C_NUMBER_TYPE) VALUES ?";
  var cli_p = [[]];
  connection.query(sql5, [cli_p], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD REFERENCE MATERIAL
  var sql6 = "INSERT INTO REFERENCE_MATERIAL (TITLE, PATH, DOC_TYPE) VALUES ?";
  var ref_mat = [[]];
  connection.query(sql6, [ref_mat], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD_REF_MAT_REGARDS_CLIENT
  var sql7 = "INSERT INTO REFERENCE_MATERIAL_REGARDS_CLIENT (C_ID, R_ID) VALUES ?";
  var ref_mat_cli = [[]];
  connection.query(sql7, [ref_mat_cli], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD_REF_MAT_REGARDS_EMPLOYEE
  var sql8 = "INSERT INTO EMPLOYEE_HAS_REFERENCE_MATERIAL (E_ID, R_ID) VALUES ?";
  var ref_mat_emp = [[]];
  connection.query(sql8, [ref_mat_emp], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD REFERENCE_MATERIAL_HAS_SUBJECT
  var sql9 = "INSERT INTO REFERENCE_MATERIAL_SUBJECT (R_ID, Subject) VALUES ?";
  var ref_mat_emp = [[]];
  connection.query(sql9, [ref_mat_emp], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD APPOINTMENTS
  var sql10 = "INSERT INTO APPOINTMENTS (DATE, TITLE, NOTES, LOCATION) VALUES ?";
  var appts = [[]];
  connection.query(sql10, [appts], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //ADD EMPLOYEE HAS APPONINTMENTS
  var sql11 = "INSERT INTO EMPLOYEE_HAS_APPOINTMENT (APP_ID, E_ID) VALUES ?";
  var eappts = [[]];
  connection.query(sql11, [eappts], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //CLIENT_HAS APPOINTMENT
  var sql12 = "INSERT INTO CLIENT_HAS_APPOINTMENT (APP_ID, C_ID) VALUES ?";
  var cappts = [[]];
  connection.query(sql12, [cappts], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //LEGAL_FILE
  var sql13 = "INSERT INTO LEGAL_FILE (FILE_STATUS, NOTES, TYPE_OF_LAW, DATE_STARTED, C_ID) VALUES ?";
  var leg_file = [[]];
  connection.query(sql13, [leg_file], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //EMPLOYEE_HAS_CASE
  var sql14 = "INSERT INTO EMPLOYEE_HAS_CASE (FILE_NUMBER, E_ID) VALUES ?";
  var leg_file = [[]];
  connection.query(sql14, [leg_file], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //DOCUMENTS
  var sql15 = "INSERT INTO DOCUMENTS (TITLE, C_ID, TYPE, CONTEXT) VALUES ?";
  var docs = [[]];
  connection.query(sql15, [docs], function (err, result){
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Trust_fund
  var sql16 = "INSERT INTO Trust_fund (FILE_NUMBER, ACCOUNT_NUMBER, FEE_PERCENTAGE, TOTAL_AMOUNT) VALUES ?";
  var tfund = [[]];
  connection.query(sql16, [tfund], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Fees
  var sql17 = "INSERT INTO Fees (FILE_NUMBER, TOTAL) VALUES ?";
  var fee = [[]];
  connection.query(sql17, [fee], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Billable_hours
  var sql18 = "INSERT INTO Billable_hours (FILE_NUMBER, NUM_OF_HOURS, PAYEE, ACTIVITY, DATE, RATE) VALUES ?";
  var bill = [[]];
  connection.query(sql18, [bill], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Disbursements
  var sql19 = "INSERT INTO Disbursements (FILE_NUMBER, PAYEE, BASE_COST, GST, TOTAL, DATE) VALUES ?";
  var dism = [[]];
  connection.query(sql19, [dism], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Paid
  var sql20 = "INSERT INTO Paid (FILE_NUMBER, AMOUNT, ACTIVITY) VALUES ?";
  var pay = [[]];
  connection.query(sql20, [pay], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Communication_Records
  var sql21 = "INSERT INTO Communication_Records (FILE_NUMBER, TITLE, CONTENT, METHOD, TIME, DATE) VALUES ?";
  var comrec = [[]];
  connection.query(sql21, [comrec], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Client_Has_Comm_Records
  var sql22 = "INSERT INTO Client_Has_Comm_Records (C_ID, FILE_NUMBER) VALUES ?";
  var clicomrec = [[]];
  connection.query(sql22, [clicomrec], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Contact_has_comm_records
  var sql23 = "INSERT INTO Contact_Has_Comm_Records (CONTACT_ID, FILE_NUMBER) VALUES ?";
  var concomrec = [[]];
  connection.query(sql23, [concomrec], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Contact
  var sql24 = "INSERT INTO Contact (NAME, NOTES, TYPE, COMPANY, EMAIL, ADDRESS) VALUES ?";
  var cont = [[]];
  connection.query(sql24, [cont], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Contact_Associated_With_Client
  var sql25 = "INSERT INTO Contact_Associated_With_Client (C_ID, CONTACT_ID) VALUES ?";
  var cont_cli = [[]];
  connection.query(sql25, [cont_cli], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //Contact_Phone
  var sql26 = "INSERT INTO Contact_Phone (CONTACT_ID, CONTACT_NUMBER, CONTACT_NUMBER_TYPE) VALUES ?";
  var cont_pho = [[]];
  connection.query(sql26, [cont_pho], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

  //CONTACT_HAS_APPOINTMENT
  var sql27 = "INSERT INTO CONTACT_HAS_APPOINTMENT (APP_ID, CONTACT_ID) VALUES ?";
  var cont_app = [[]];
  connection.query(sql27, [cont_app], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });

});

app.listen(3307);